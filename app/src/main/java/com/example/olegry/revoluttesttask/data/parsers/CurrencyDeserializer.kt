package com.example.olegry.revoluttesttask.data.parsers

import com.example.olegry.revoluttesttask.models.Currency
import com.example.olegry.revoluttesttask.models.CurrencyResponse
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

class CurrencyDeserializer : JsonDeserializer<CurrencyResponse> {

    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext) = with(json.asJsonObject) {
        val base = this["base"].asString
        val date = this["date"].asString
        val ratesJson = this["rates"].asJsonObject

        val currenciesJson = ratesJson.entrySet()
        val rates = mutableListOf<Currency>()

        for ((key, value) in currenciesJson) {
            rates.add(Currency(key, value.asDouble))
        }

        return@with CurrencyResponse(base, date, rates)
    }
}