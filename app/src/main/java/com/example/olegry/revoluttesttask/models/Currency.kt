package com.example.olegry.revoluttesttask.models

data class Currency(
        val name: String,
        val rate: Double
)