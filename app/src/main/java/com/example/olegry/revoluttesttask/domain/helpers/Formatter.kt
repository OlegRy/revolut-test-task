package com.example.olegry.revoluttesttask.domain.helpers

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

interface Formatter {

    fun formatCurrency(currency: Double): String
    fun parseCurrency(amount: String): Double
}

class ReadableFormatter : Formatter {

    private val decimalFormat: DecimalFormat

    init {
        val decimalFormatSymbols = DecimalFormatSymbols().apply {
            decimalSeparator = ','
        }
        decimalFormat = DecimalFormat("#.##", decimalFormatSymbols)
    }

    override fun formatCurrency(currency: Double) = decimalFormat.format(currency)

    override fun parseCurrency(amount: String) = decimalFormat.parse(amount).toDouble()
}