package com.example.olegry.revoluttesttask.data.schedulers

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface SchedulersProvider {
    val io: Scheduler
    val ui: Scheduler
    val computation: Scheduler
}

class AppSchedulers : SchedulersProvider {
    override val io = Schedulers.io()
    override val ui = AndroidSchedulers.mainThread()
    override val computation = Schedulers.computation()
}