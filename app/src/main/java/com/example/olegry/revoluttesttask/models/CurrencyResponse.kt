package com.example.olegry.revoluttesttask.models

data class CurrencyResponse(
        val base: String,
        val date: String,
        val rates: List<Currency>
)