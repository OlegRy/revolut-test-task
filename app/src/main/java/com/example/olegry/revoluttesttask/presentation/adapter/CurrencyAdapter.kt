package com.example.olegry.revoluttesttask.presentation.adapter

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.example.olegry.revoluttesttask.R
import com.example.olegry.revoluttesttask.models.CurrencyAdapterItem

class CurrencyAdapter(
        private val items: MutableList<CurrencyAdapterItem>,
        private val callback: AdapterCallback
) : RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder>() {

    interface AdapterCallback {
        fun onItemClick(position: Int)
        fun onBaseCurrencyAmountChanged(newAmount: String, position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CurrencyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_currency, parent, false),
            callback
    )

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position)
        } else {
            for (payload in payloads) {
                if (payload == AMOUNT_UPDATE) {
                    holder.bindAmount(items[position].amount)
                }
            }
        }
    }

    fun getItem(position: Int) = items[position]

    fun update(newList: List<CurrencyAdapterItem>, detectMoves: Boolean = false) {
        val diffCallback = CurrencyDiffCallback(items.toList(), newList.toList())
        val diffResult = DiffUtil.calculateDiff(diffCallback, detectMoves)
        items.run {
            clear()
            addAll(newList)
        }
        diffResult.dispatchUpdatesTo(this)
    }

    class CurrencyViewHolder(itemView: View, callback: AdapterCallback) : RecyclerView.ViewHolder(itemView) {

        private val tvCurrencyName = itemView.findViewById<TextView>(R.id.tvCurrencyName)
        private val etAmount = itemView.findViewById<EditText>(R.id.etAmount)

        init {
            itemView.setOnClickListener { callback.onItemClick(adapterPosition) }
            etAmount.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable) {
                    callback.onBaseCurrencyAmountChanged(s.toString(), adapterPosition)
                }

                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            })
        }

        fun bind(item: CurrencyAdapterItem) = with(item) {
            tvCurrencyName.text = name
            etAmount.setText(amount)
        }

        fun bindAmount(amount: String) {
            etAmount.setText(amount)
        }
    }

    class CurrencyDiffCallback(
            private val source: List<CurrencyAdapterItem>,
            private val target: List<CurrencyAdapterItem>
    ) : DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) = source[oldItemPosition].name == target[newItemPosition].name

        override fun getOldListSize() = source.size

        override fun getNewListSize() = target.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) = source[oldItemPosition].amount == target[newItemPosition].amount

        override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
            val oldItem = source[oldItemPosition]
            val newItem = target[newItemPosition]

            if (oldItem.amount != newItem.amount && newItemPosition != 0) {
                return AMOUNT_UPDATE
            }
            return NO_UPDATES
        }
    }

    companion object {
        const val AMOUNT_UPDATE = 0
        const val NO_UPDATES = 1
    }
}