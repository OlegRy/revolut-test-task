package com.example.olegry.revoluttesttask.presentation

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.example.olegry.revoluttesttask.App
import com.example.olegry.revoluttesttask.R
import com.example.olegry.revoluttesttask.models.CurrencyAdapterItem
import com.example.olegry.revoluttesttask.presentation.adapter.CurrencyAdapter
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : MvpAppCompatActivity(), MainView, CurrencyAdapter.AdapterCallback {

    @Inject
    @InjectPresenter
    lateinit var presenter: MainPresenter

    private val adapter: CurrencyAdapter by lazy { CurrencyAdapter(mutableListOf(), this) }

    @ProvidePresenter
    fun providePresenter() = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        app().mainComponent().inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initRecyclerView()
    }

    override fun onBackPressed() {
        app().clearMainComponent()
        super.onBackPressed()
    }

    override fun updateItems(items: List<CurrencyAdapterItem>, scrollToStart: Boolean) {
        adapter.update(items, scrollToStart)
        if (scrollToStart) rvItems.smoothScrollToPosition(0)
    }

    override fun showError(error: Throwable) {
        Toast.makeText(this, R.string.error_text, Toast.LENGTH_SHORT).show()
    }

    override fun onItemClick(position: Int) {
        presenter.onItemClick(adapter.getItem(position), position)
    }

    override fun onBaseCurrencyAmountChanged(newAmount: String, position: Int) {
        presenter.onBaseCurrencyAmountChanged(newAmount, position)
    }

    private fun app() = applicationContext as App

    private fun initRecyclerView() = with(rvItems) {
        layoutManager = LinearLayoutManager(this@MainActivity)
        adapter = this@MainActivity.adapter
    }
}
