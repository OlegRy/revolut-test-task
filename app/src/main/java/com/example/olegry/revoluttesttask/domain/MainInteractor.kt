package com.example.olegry.revoluttesttask.domain

import com.example.olegry.revoluttesttask.domain.helpers.Formatter
import com.example.olegry.revoluttesttask.models.CurrencyAdapterItem
import com.example.olegry.revoluttesttask.repositories.MainRepository
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

interface MainInteractor {

    fun getCurrencyList(): Single<List<CurrencyAdapterItem>>
    fun updateAmounts(baseCurrencyAmount: String): Single<List<CurrencyAdapterItem>>
    fun updateBaseCurrency(newCurrency: CurrencyAdapterItem): Single<List<CurrencyAdapterItem>>
}

class MainInteractorImpl @Inject constructor(
        private val repository: MainRepository,
        private val formatter: Formatter
) : MainInteractor {

    private var baseCurrency = CurrencyAdapterItem("EUR", "100")
    private var downloadedRates = mutableListOf<CurrencyAdapterItem>()

    override fun getCurrencyList() = repository.getCurrencyList(baseCurrency.name)
            .doOnNext { baseCurrency.name = it.base }
            .map { it.rates }
            .flatMap { Observable.fromIterable(it) }
            .map {
                CurrencyAdapterItem(it.name,
                        formatter.formatCurrency(it.rate * formatter.parseCurrency(baseCurrency.amount)), it.rate)
            }
            .toList()
            .doOnSuccess {
                updateDownloadedRates(it.toList())
                it.add(0, baseCurrency.copy())
            }

    override fun updateAmounts(baseCurrencyAmount: String): Single<List<CurrencyAdapterItem>> {
        return Observable.fromCallable { baseCurrency.amount = if (baseCurrencyAmount.isBlank()) "0" else baseCurrencyAmount }
                .flatMap { Observable.fromIterable(downloadedRates) }
                .map {
                    val newCurrency = formatter.formatCurrency(it.rate * formatter.parseCurrency(baseCurrency.amount))
                    return@map it.copy(amount = newCurrency)
                }
                .toList()
                .doOnSuccess {
                    updateDownloadedRates(it.toList())
                    it.add(0, baseCurrency.copy())
                }
    }

    override fun updateBaseCurrency(newCurrency: CurrencyAdapterItem) = Observable.fromIterable(downloadedRates)
            .filter { it != newCurrency }
            .map { it.copy() }
            .toList()
            .doOnSuccess {
                it.add(0, baseCurrency.copy())
                updateDownloadedRates(it.toList())
                it.add(0, newCurrency.copy())
                baseCurrency = newCurrency.copy()
            }

    @Synchronized
    private fun updateDownloadedRates(rates: List<CurrencyAdapterItem>) {
        downloadedRates.run {
            clear()
            addAll(rates)
        }
    }
}