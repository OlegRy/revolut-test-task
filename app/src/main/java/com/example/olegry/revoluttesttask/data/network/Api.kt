package com.example.olegry.revoluttesttask.data.network

import com.example.olegry.revoluttesttask.models.CurrencyResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("/latest")
    fun latest(@Query("base") baseCurrency: String): Observable<Response<CurrencyResponse>>
}