package com.example.olegry.revoluttesttask.models

data class CurrencyAdapterItem(
        var name: String,
        var amount: String,
        val rate: Double = 1.0
)