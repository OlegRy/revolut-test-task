package com.example.olegry.revoluttesttask.di.main

import com.example.olegry.revoluttesttask.data.network.Api
import com.example.olegry.revoluttesttask.di.scopes.ActivityScope
import com.example.olegry.revoluttesttask.domain.MainInteractor
import com.example.olegry.revoluttesttask.domain.MainInteractorImpl
import com.example.olegry.revoluttesttask.domain.helpers.Formatter
import com.example.olegry.revoluttesttask.repositories.MainRepository
import com.example.olegry.revoluttesttask.repositories.MainRepositoryImpl
import dagger.Module
import dagger.Provides

@Module
class MainModule {

    @Provides
    @ActivityScope
    fun provideRepository(api: Api): MainRepository = MainRepositoryImpl(api)

    @Provides
    @ActivityScope
    fun provudeInteractor(repository: MainRepository, formatter: Formatter): MainInteractor = MainInteractorImpl(repository, formatter)
}
