package com.example.olegry.revoluttesttask.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.olegry.revoluttesttask.data.schedulers.SchedulersProvider
import com.example.olegry.revoluttesttask.domain.MainInteractor
import com.example.olegry.revoluttesttask.models.CurrencyAdapterItem
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@InjectViewState
class MainPresenter @Inject constructor(
        private val interactor: MainInteractor,
        private val schedulers: SchedulersProvider
) : MvpPresenter<MainView>() {

    private val disposables = CompositeDisposable()

    override fun onFirstViewAttach() {
        Observable.interval(0, 1, TimeUnit.SECONDS)
                .flatMapSingle { interactor.getCurrencyList() }
                .repeat()
                .subscribeOn(schedulers.io)
                .observeOn(schedulers.ui)
                .subscribe(
                        { viewState.updateItems(it) },
                        { viewState.showError(it) }
                ).connect()
    }

    override fun onDestroy() {
        disposables.dispose()
        super.onDestroy()
    }

    fun onItemClick(item: CurrencyAdapterItem, position: Int) {
        if (position != 0) {
            interactor.updateBaseCurrency(item)
                    .subscribeOn(schedulers.io)
                    .observeOn(schedulers.ui)
                    .subscribe(
                            { viewState.updateItems(it, true) },
                            { viewState.showError(it) }
                    ).connect()
        }
    }

    fun onBaseCurrencyAmountChanged(newAmount: String, position: Int) {
        if (position == 0) {
            interactor.updateAmounts(newAmount)
                    .subscribeOn(schedulers.computation)
                    .observeOn(schedulers.ui)
                    .subscribe(
                            { viewState.updateItems(it) },
                            { viewState.showError(it) }
                    ).connect()
        }
    }

    private fun Disposable.connect() {
        disposables.add(this)
    }
}