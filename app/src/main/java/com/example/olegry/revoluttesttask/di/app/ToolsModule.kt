package com.example.olegry.revoluttesttask.di.app

import com.example.olegry.revoluttesttask.data.schedulers.AppSchedulers
import com.example.olegry.revoluttesttask.data.schedulers.SchedulersProvider
import com.example.olegry.revoluttesttask.domain.helpers.Formatter
import com.example.olegry.revoluttesttask.domain.helpers.ReadableFormatter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ToolsModule {

    @Provides
    @Singleton
    fun provideSchedulers(): SchedulersProvider = AppSchedulers()

    @Provides
    @Singleton
    fun provideFormatter(): Formatter = ReadableFormatter()
}