package com.example.olegry.revoluttesttask.di.main

import com.example.olegry.revoluttesttask.di.app.AppComponent
import com.example.olegry.revoluttesttask.di.scopes.ActivityScope
import com.example.olegry.revoluttesttask.presentation.MainActivity
import dagger.Component

@Component(dependencies = [AppComponent::class], modules = [MainModule::class])
@ActivityScope
interface MainComponent {
    fun inject(activity: MainActivity)

    class Initializer private constructor() {
        companion object {
            fun init(appComponent: AppComponent) = DaggerMainComponent.builder()
                    .appComponent(appComponent)
                    .build()
        }
    }
}