package com.example.olegry.revoluttesttask.presentation

import com.arellomobile.mvp.MvpView
import com.example.olegry.revoluttesttask.models.CurrencyAdapterItem

interface MainView : MvpView {
    fun updateItems(items: List<CurrencyAdapterItem>, scrollToStart: Boolean = false)
    fun showError(error: Throwable)
}