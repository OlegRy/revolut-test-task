package com.example.olegry.revoluttesttask

import android.app.Application
import com.example.olegry.revoluttesttask.di.app.AppComponent
import com.example.olegry.revoluttesttask.di.main.MainComponent

class App : Application() {

    private lateinit var appComponent: AppComponent
    private var mainComponent: MainComponent? = null

    override fun onCreate() {
        super.onCreate()
        appComponent = AppComponent.Initializer.init(this)
        appComponent.inject(this)
    }

    fun mainComponent(): MainComponent {
        if (mainComponent == null) {
            mainComponent = MainComponent.Initializer.init(appComponent)
        }
        return mainComponent!!
    }

    fun clearMainComponent() {
        mainComponent = null
    }
}