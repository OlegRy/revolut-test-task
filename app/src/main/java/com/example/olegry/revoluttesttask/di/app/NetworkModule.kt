package com.example.olegry.revoluttesttask.di.app

import com.example.olegry.revoluttesttask.data.network.Api
import com.example.olegry.revoluttesttask.data.parsers.CurrencyDeserializer
import com.example.olegry.revoluttesttask.models.CurrencyResponse
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideGson() = GsonBuilder()
            .registerTypeAdapter(CurrencyResponse::class.java, CurrencyDeserializer())
            .create()

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson) = Retrofit.Builder()
            .baseUrl("https://revolut.duckdns.org")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

    @Provides
    @Singleton
    fun provideApi(retrofit: Retrofit) = retrofit.create(Api::class.java)
}