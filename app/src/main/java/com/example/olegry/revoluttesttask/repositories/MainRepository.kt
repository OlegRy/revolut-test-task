package com.example.olegry.revoluttesttask.repositories

import com.example.olegry.revoluttesttask.data.network.Api
import com.example.olegry.revoluttesttask.models.CurrencyResponse
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import retrofit2.Response
import javax.inject.Inject

interface MainRepository {

    fun getCurrencyList(base: String): Observable<CurrencyResponse>
}

class MainRepositoryImpl @Inject constructor(
        private val api: Api
) : MainRepository {

    override fun getCurrencyList(base: String) = api.latest(base)
            .compose(handleResponse())

    private fun <T> handleResponse(): ObservableTransformer<Response<T>, T> =
            ObservableTransformer {
                it.flatMap { response ->
                    if (response.isSuccessful) Observable.just(response.body())
                    else Observable.error(RuntimeException())
                }
            }
}