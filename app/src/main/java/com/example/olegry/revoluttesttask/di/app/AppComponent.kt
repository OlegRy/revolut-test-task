package com.example.olegry.revoluttesttask.di.app

import android.content.Context
import com.example.olegry.revoluttesttask.App
import com.example.olegry.revoluttesttask.data.network.Api
import com.example.olegry.revoluttesttask.data.schedulers.SchedulersProvider
import com.example.olegry.revoluttesttask.domain.helpers.Formatter
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class, ToolsModule::class])
interface AppComponent {

    fun inject(app: App)
    fun api(): Api
    fun schedulers(): SchedulersProvider
    fun formatter(): Formatter

    @Component.Builder
    interface Builder {
        fun build(): AppComponent
        @BindsInstance fun withAppContext(appContext: Context): Builder
    }

    class Initializer private constructor() {
        companion object {
            fun init(appContext: Context) = DaggerAppComponent.builder()
                    .withAppContext(appContext)
                    .build()
        }
    }
}